import win32com.client
from win32com.client import constants
from win32com.client import Dispatch as disp
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import tostring

si = win32com.client.Dispatch("XSI.Application")
from siutils import si
from siutils import log # LogMessage
from siutils import disp # win32com.client.Dispatch
from siutils import C # win32com.client.constants

import os, sys, json

log = si.LogMessage
oSel = si.Selection
oSel2 = si.GetValue("selectionlist")

####################################################################
SPEC_VERSION = 1.0
####################################################################

class FUFBX(object):
    """This is a scriptable version of the exporter"""
    def __init__(self):
        super(FUFBX, self).__init__()
        
    
    def exportCamData(self, outFile, startFrame, endFrame, step, camList, department=None, comments=None):
        if outFile != "": # If there is text in the file out text field.
            if camList: # If there are cameras selected.
                if startFrame and endFrame: # If we have a start and end frame... then lift offfff...
                    log("Working...")

                    log(startFrame)

                    startFrame = float(startFrame)
                    endFrame = float(endFrame)
                    step = float(step)

                    log(startFrame)

                    # Turn logging back on.
                    self.logOff()

                    camOut = {}
                    
                    step = 1

                    # Loop through each camera.
                    for sel in camList:
                        log(type(sel))
                        log(sel.Name)

                        # Plot camera before exporting data. This constraint method is to bake the camera's global transforms into kine.local for the plot.
                        tmpBakedCam = si.Duplicate(sel.Name, "", 2, 1, 1, 0, 0, 1, 0, 1, "", "", "", "", "", "", "", "", "", "", 0)
                        si.ParentObj("Scene_Root", tmpBakedCam)
                        si.ApplyCns("Pose", tmpBakedCam, sel.Name, "")
                        tmpString = str(tmpBakedCam) + ".kine.local.posx", str(tmpBakedCam) + ".kine.local.posy", str(tmpBakedCam) + ".kine.local.posz", str(tmpBakedCam) + ".kine.local.rotx", str(tmpBakedCam) + ".kine.local.roty", str(tmpBakedCam) + ".kine.local.rotz"
                        si.PlotAndApplyActions(tmpString, "plot", startFrame, endFrame, "", 20, 2, "", "", "", "", True, True)
                        si.RemoveAllCns(str(tmpBakedCam), "")

                        # Create camera dict.
                        camParm = {}
                        camOut[sel.Name] = camParm

                        # Get specific params. Might be a better way to just explicitly define what gets output.
                        # Primitive tab.
                        camParm["aspect"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.aspect", False).Value
                        camParm["pixelRatio"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.pixelratio", False).Value
                        camParm["clippingNear"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.near", False).Value
                        camParm["clippingFar"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.far", False).Value
                        camParm["rotOrder"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".kine.local.rotorder", False).Value # 0 = XYZ, 1 = XZY, 2 = YXZ, 3 = YZX, 4 = ZXY, 5 = ZYX

                        # Projection Plane tab.
                        camParm["focalLength"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.projplanedist", False).Value
                        camParm["apertureWidth"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.projplanewidth", False).Value * 25.4 # Conversion to mm from inches.
                        camParm["apertureHeight"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.projplaneheight", False).Value * 25.4 # Conversion to mm from inches.

                        # Lens Shaders tab.
                        # camParm["lensShader"] = si.Dictionary.GetObject(str(sel.text()) + ".camera.lensshader", False).Value

                        # Misc.
                        camParm["startFrame"] = startFrame
                        camParm["endFrame"] = endFrame
                        camParm["step"] = step

                        # Metadata.
                        metadata = {}

                        # Get date and time.
                        import datetime
                        t = datetime.datetime.now()
                        now = t.strftime("%Y-%m-%d %H:%M")
                        metadata["dateCreated"] = now

                        # Get UNIX time.
                        import time
                        timestamp = int(time.time())
                        metadata["unixTime"] = timestamp

                        metadata["software"] = si.Name + " " + si.Version()
                        # metadata["published"] = "true" # Use bool
                        metadata["createdBy"] = os.environ["USERNAME"]
                        metadata["department"] = department
                        metadata["comments"] = comments
                        metadata["specVersion"] = SPEC_VERSION

                        camParm["meta"] = metadata

                        disp = win32com.client.Dispatch # ADDED for module version. Stupid python.

                        myCam = si.ActiveSceneRoot.FindChild(str(tmpBakedCam))
                        px = myCam.Kinematics.Local.Parameters("PosX")
                        pxFcurve = px.Source
                        py = myCam.Kinematics.Local.Parameters("PosY")
                        pyFcurve = py.Source
                        pz = myCam.Kinematics.Local.Parameters("PosZ")
                        pzFcurve = pz.Source

                        rx = myCam.Kinematics.Local.Parameters("RotX")
                        rxFcurve = rx.Source
                        ry = myCam.Kinematics.Local.Parameters("RotY")
                        ryFcurve = ry.Source
                        rz = myCam.Kinematics.Local.Parameters("RotZ")
                        rzFcurve = rz.Source

                        anim = {}

                        # Loop through all frames.
                        frame = startFrame
                        while frame <= endFrame:
                            frameString = str(frame)
                            # log("/// Frame: " + str(frame))

                            xforms = {}

                            xforms["px"] = disp(pxFcurve).Eval(frame)
                            xforms["py"] = disp(pyFcurve).Eval(frame)
                            xforms["pz"] = disp(pzFcurve).Eval(frame)
                            xforms["rx"] = disp(rxFcurve).Eval(frame)
                            xforms["ry"] = disp(ryFcurve).Eval(frame)
                            xforms["rz"] = disp(rzFcurve).Eval(frame)
    
                            anim[frameString] = xforms

                            frame += step


                        # # Delete temp camera after plot.
                        si.DeleteObj(tmpBakedCam)
                        
                        camParm["anim"] = anim

                    # Write out file.
                    # outFile = self.ui.fileOutLocationLineEdit.text()
                    with open(outFile, "w") as outFile:
                        json.dump(camOut, outFile, sort_keys=True, indent=4)

                    # Turn logging back on.
                    self.logOn()
            
                    log("------------------------- Script has completed successfully! ------------------------")
                    return True
                else:
                    log("Please specify a start and end frame range to bake.", 4)
                    return False
            else:
                log("Please select one or more cameras and try again.", 4)
                return False
        else:
            log("Please provide an output file. eg. /tmp/cameraFile.cam", 4)
            return False



    def logOff(msg = "Logging has been turned off"):
        """Turn off PPG inspection for the duration of the function."""
        pref = "preferences.Interaction.autoinspect" # "preferences.scripting.cmdlog, preferences.scripting.msglog, preferences.scripting.msglogverbose, preferences.scripting.cmdlog, preferences.Interaction.autoinspect"
        log(msg, 32)
        si.SetValue(pref, False)

    def logOn(msg = "Logging has been turned on"):
        """Turn on PPG inspection back on after performing a task."""
        pref = "preferences.Interaction.autoinspect" # "preferences.scripting.cmdlog, preferences.scripting.msglog, preferences.scripting.msglogverbose, preferences.scripting.cmdlog, preferences.Interaction.autoinspect"
        si.SetValue(pref, True)
        log(msg, 32)
"""
APPLICATION:    JSON Cam
VERSION:        0.1
DATE:           June 27, 2013
AUTHOR:         Gene Crucean
DESCRIPTION:    JSON Cam is a tool that exports a json file for easy cross application camera transfer.

NOTES:
Based on SPEC: 1.0

- It exports key:value (keyframe:value) as an array in posx, posy, posz, rotx, roty, rotz order.

- apertureHeight and apertureWidth are the sensor size in inches. (Always store as millimeters in the file. Do conversions on export/import if needed.)
- aspect: duh.
- clippingNear and clippingFar: Clipping planes.
- focalLength: sheesh... c'mon now.
- pixelRatio: Pretty self explanatory.
- rotOrder: 0 = XYZ, 1 = XZY, 2 = YXZ, 3 = YZX, 4 = ZXY, 5 = ZYX. Default is 0.

Assumptions:

- Camera AOV should always horizontal.
- Cameras are always perspective and not ortho.

Thanks:
Eric Thivierge (ethivierge@gmail.com).


"""

import win32com.client
from win32com.client import constants
from win32com.client import Dispatch as disp
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import tostring

import base64
import traceback

#-------------------------- Import Python base modules.
import os
import sys
# import simplejson
import json
from pprint import pprint

# import JSONCamHelpers

#-------------------------- Python si initial setup.
null = None
false = 0
true = 1
si = Application
oRoot = si.ActiveSceneRoot
log = si.LogMessage
oSel = si.Selection
oSel2 = si.GetValue("selectionlist")
oScn = si.ActiveProject.ActiveScene
oPass = si.ActiveProject.ActiveScene.ActivePass
oProjPath = Application.InstallationPath(0)
CF = si.GetValue("PlayControl.Current")
FF = si.GetValue("PlayControl.In")
LF = si.GetValue("PlayControl.Out")

minKey = 0
maxKey = 0
frameRange = ""

SPEC_VERSION = 1.0

#--------------------------
 
import sip
from PyQt4 import uic
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtGui import QDialog
from PyQt4.QtGui import QWidget
from PyQt4.QtGui import QApplication
from PyQt4.QtGui import QFileDialog

# sip.setapi('QString', 2)


__version__ = 0.1 # This must be compatible with the float type. eg. NOT 0.0.1



class JSONCam(QDialog):
    def __init__( self, uifilepath, parent ):
        QDialog.__init__(self, parent)
        QApplication.setStyle(QtGui.QStyleFactory.create('Plastique'))
        self.ui = uic.loadUi( uifilepath, self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose) # Ensures that the app is deleted when closed.
        self.setConnections()
        self.init_UI()
        
    def setConnections(self):
        self.ui.importButton.released.connect(self.importButtonExecute)
        self.ui.exportButton.released.connect(self.exportButtonExecute)
        self.ui.importBrowseButton.released.connect(self.setFileInLocation)
        self.ui.exportBrowseButton.released.connect(self.setFileOutLocation)
        
    def init_UI(self):
        self.getSceneCameras()

        # # Get keyframe range for current camera.
        # frameRange = self.getKeyframeRange(str(sel.text()))
        # log("//////////////: " + str(frameRange))

        # # Set text into start/end frame range ui elements.
        # self.ui.startFrameRangeLineEdit.setText(frameRange[0])
        # self.ui.endFrameRangeLineEdit.setText(frameRange[1])

        
    def importButtonExecute(self):
        fileIn = self.ui.fileInLocationLineEdit.text()
        self.importCamData(fileIn)
        

    def importCamData(self, fileIn):
        #log("------------------------- Script has started ------------------------")
        if fileIn:
            self.ui.importOutputMessageLabel.setText("Working...")
        
            self.logOff()

            # Get json data from file.
            jsonData = open(fileIn)
            data = json.load(jsonData)
            jsonData.close()

            si.DeselectAll()
            jsonCamModel = si.CreateModel("", "JSONCam", "", "")
            # log(oObj.FullName)

            # Loops through each camera.
            for camera, value1 in data.iteritems():
                # log("camera: " + str(camera) + " value1: " + str(value1))
                
                # Create camera
                # root = si.GetPrimCamera("", str(camera), "JSONCam", prim, newCam, interest)
                newCamModel = si.Dictionary.GetObject("JSONCam", False)
                newCam = newCamModel.AddCamera("Camera", str(camera));
                Application.DeleteObj("JSONCam.CameraInterest")

                # Make sure SPEC_VERSION is the same as the specVersion specified in the .cam file.
                if SPEC_VERSION != data[camera]["meta"]["specVersion"]:
                    self.ui.importOutputMessageLabel.setText("Spec version in .cam file doesn't match importers version.")
                    log("Spec version in .cam file doesn't match importers version. Make sure you have the latest plugins from www.genecrucean.com", 2)
                    si.DeleteObj('B:' + str(newCamModel))
                    return False

                # Set values for each camera.
                # Primitive tab.
                si.SetValue("JSONCam." + newCam.Name + ".camera.aspect", data[camera]["aspect"], "")
                si.SetValue("JSONCam." + newCam.Name + ".camera.pixelratio", data[camera]["pixelRatio"], "")
                si.SetValue("JSONCam." + newCam.Name + ".camera.near", data[camera]["clippingNear"], "")
                si.SetValue("JSONCam." + newCam.Name + ".camera.far", data[camera]["clippingFar"], "")

                # Projection Plane tab.
                si.SetValue("JSONCam." + newCam.Name + ".camera.projplane", True, "")
                si.SetValue("JSONCam." + newCam.Name + ".camera.projplanelockaspect", False, "")
                si.SetValue("JSONCam." + newCam.Name + ".camera.projplaneheight", (data[camera]["apertureHeight"]) / 25.4, "") # Conversion to inches from mm.
                si.SetValue("JSONCam." + newCam.Name + ".camera.projplanewidth", (data[camera]["apertureWidth"]) / 25.4, "") # Conversion to inches from mm.
                si.SetValue("JSONCam." + newCam.Name + ".camera.projplanedist", data[camera]["focalLength"], "")

                # Local kinematics.
                si.SetValue("JSONCam." + newCam.Name + ".kine.local.rotorder", int(data[camera]["rotOrder"]), "")

                # si.SIAddArrayElement("Camera_TEST.camera.lensshader")
                # si.SIApplyShaderToCnxPoint2("Softimage.equidistantStereoCamera.1.0", "Camera_TEST.camera.Item", "", "")

                # Add annotation for metadata.
                metaTester = si.Dictionary.GetObject("JSONCam.metadata", False)
                if not metaTester:
                    si.AddProp("Annotation", newCamModel, "", "metadata", "")
                    metaString = ""
                    for k in data[camera]["meta"]:
                        metaString += "%s: %s\r\n" % (k, data[camera]["meta"][k])

                    si.SetValue("JSONCam.metadata.text", metaString, "")

                # Get start, end and step values for iterating later.
                startFrame = data[camera]["startFrame"]
                endFrame = data[camera]["endFrame"]
                step = data[camera]["step"]

                # Loops through each cameras params.
                for param, value2 in data[camera].iteritems():
                    # log("param: " + str(param) + " value2: " + str(value2))

                    # log(newCamModel.Name)
                    # log(newCam.Name)

                    # Loops through time then values.
                    if param == "anim":
                        curveType = 30
                        px = newCam.Kinematics.Local.Parameters("PosX")
                        pxCurve = px.AddFCurve2(None, curveType)

                        py = newCam.Kinematics.Local.Parameters("PosY")
                        pyCurve = py.AddFCurve2(None, curveType)

                        pz = newCam.Kinematics.Local.Parameters("PosZ")
                        pzCurve = pz.AddFCurve2(None, curveType)

                        rx = newCam.Kinematics.Local.Parameters("RotX")
                        rxCurve = rx.AddFCurve2(None, curveType)
                        
                        ry = newCam.Kinematics.Local.Parameters("RotY")
                        ryCurve = ry.AddFCurve2(None, curveType)
                        
                        rz = newCam.Kinematics.Local.Parameters("RotZ")
                        rzCurve = rz.AddFCurve2(None, curveType)

                        # Loops through frames in "anim".
                        for f in range(startFrame, endFrame + 1):
                            # log(f)
                            f2 = round(f, 2)
                            frame = str(f2)
                            # log(frame)

                            # Loops through frame params.
                            for p, v in data[camera][param][frame].iteritems():
                                # log("param: " + str(p) + " value: " + str(v))

                                if p == "px":
                                    pxCurve.AddKey(f, v)
                                elif p == "py":
                                    pyCurve.AddKey(f, v)
                                elif p == "pz":
                                    pzCurve.AddKey(f, v)
                                elif p == "rx":
                                    rxCurve.AddKey(f, v)
                                elif p == "ry":
                                    ryCurve.AddKey(f, v)
                                elif p == "rz":
                                    rzCurve.AddKey(f, v)
                                else:
                                    self.ui.importOutputMessageLabel.setText("This .cam file contains animated parameters that aren't supported by this version of the importer.")
                                    log("This .cam file contains animated parameters that aren't supported by this version of the importer.", 4)
                                    return False



            self.logOn()
            
            log("------------------------- Script has completed successfully! ------------------------")
            self.ui.importOutputMessageLabel.setText("Cameras have been imported successfully!")
            return True
        else:
            self.ui.importOutputMessageLabel.setText("Please provide a file to import. eg. /tmp/cameraFile.cam")
            return False


    def exportButtonExecute(self):
        outFile = str(self.ui.fileOutLocationLineEdit.text())
        startFrame = float(self.ui.startFrameLineEdit.text())
        endFrame = float(self.ui.endFrameLineEdit.text())
        step = int(self.ui.stepLineEdit.text())
        camList = self.ui.cameraList.selectedItems()
        department = str(self.ui.departmentLineEdit.text())
        comments = str(self.ui.commentsLineEdit.text())

        self.exportCamData(outFile, startFrame, endFrame, step, camList, department, comments)

    def exportCamData(self, outFile, startFrame, endFrame, step, camList, department=None, comments=None):
        if outFile != "": # If there is text in the file out text field.
            if camList: # If there are cameras selected.
                if startFrame and endFrame: # If we have a start and end frame... then lift offfff...
                    self.ui.exportOutputMessageLabel.setText("Working...")
                    self.logOff()

                    camOut = {}
                    
                    step = 1

                    # Loop through each camera.
                    for sel in camList:
                        # log(sel.text())

                        # Plot camera before exporting data. This constraint method is to bake the camera's global transforms into kine.local for the plot.
                        tmpBakedCam = si.Duplicate(str(sel.text()), "", 2, 1, 1, 0, 0, 1, 0, 1, "", "", "", "", "", "", "", "", "", "", 0)
                        # log(tmpBakedCam)
                        si.ParentObj("Scene_Root", tmpBakedCam)
                        si.ApplyCns("Pose", tmpBakedCam, str(sel.text()), "")
                        tmpString = str(tmpBakedCam) + ".kine.local.posx", str(tmpBakedCam) + ".kine.local.posy", str(tmpBakedCam) + ".kine.local.posz", str(tmpBakedCam) + ".kine.local.rotx", str(tmpBakedCam) + ".kine.local.roty", str(tmpBakedCam) + ".kine.local.rotz"
                        # log(tmpString)
                        si.PlotAndApplyActions(tmpString, "plot", startFrame, endFrame, "", 20, 2, "", "", "", "", True, True)
                        si.RemoveAllCns(str(tmpBakedCam), "")

                        # Create camera dict.
                        camParm = {}
                        camOut[str(sel.text())] = camParm

                        # Get specific params. Might be a better way to just explicitly define what gets output.
                        # Primitive tab.
                        camParm["aspect"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.aspect", False).Value
                        camParm["pixelRatio"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.pixelratio", False).Value
                        camParm["clippingNear"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.near", False).Value
                        camParm["clippingFar"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.far", False).Value
                        camParm["rotOrder"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".kine.local.rotorder", False).Value # 0 = XYZ, 1 = XZY, 2 = YXZ, 3 = YZX, 4 = ZXY, 5 = ZYX

                        # Projection Plane tab.
                        camParm["focalLength"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.projplanedist", False).Value
                        camParm["apertureWidth"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.projplanewidth", False).Value * 25.4 # Conversion to mm from inches.
                        camParm["apertureHeight"] = si.Dictionary.GetObject(str(tmpBakedCam) + ".camera.projplaneheight", False).Value * 25.4 # Conversion to mm from inches.

                        # Lens Shaders tab.
                        # camParm["lensShader"] = si.Dictionary.GetObject(str(sel.text()) + ".camera.lensshader", False).Value

                        # Misc.
                        camParm["startFrame"] = startFrame
                        camParm["endFrame"] = endFrame
                        camParm["step"] = step

                        # Metadata.
                        metadata = {}

                        # Get date and time.
                        import datetime
                        t = datetime.datetime.now()
                        now = t.strftime("%Y-%m-%d %H:%M")
                        metadata["dateCreated"] = now

                        # Get UNIX time.
                        import time
                        timestamp = int(time.time())
                        metadata["unixTime"] = timestamp

                        metadata["software"] = si.Name + " " + si.Version()
                        # metadata["published"] = "true" # Use bool
                        metadata["createdBy"] = os.environ["USERNAME"]
                        metadata["department"] = department
                        metadata["comments"] = comments
                        metadata["specVersion"] = SPEC_VERSION

                        camParm["meta"] = metadata

                        myCam = si.ActiveSceneRoot.FindChild(str(tmpBakedCam))
                        px = myCam.Kinematics.Local.Parameters("PosX")
                        pxFcurve = px.Source
                        py = myCam.Kinematics.Local.Parameters("PosY")
                        pyFcurve = py.Source
                        pz = myCam.Kinematics.Local.Parameters("PosZ")
                        pzFcurve = pz.Source

                        rx = myCam.Kinematics.Local.Parameters("RotX")
                        rxFcurve = rx.Source
                        ry = myCam.Kinematics.Local.Parameters("RotY")
                        ryFcurve = ry.Source
                        rz = myCam.Kinematics.Local.Parameters("RotZ")
                        rzFcurve = rz.Source

                        anim = {}

                        # Loop through all frames.
                        # for i in range(startFrame, endFrame + 1):
                        frame = startFrame
                        while frame <= endFrame:
                            frameString = str(frame)
                            # log("/// Frame: " + str(frame))

                            xforms = {}

                            xforms["px"] = pxFcurve.Eval(frame)
                            xforms["py"] = pyFcurve.Eval(frame)
                            xforms["pz"] = pzFcurve.Eval(frame)
                            xforms["rx"] = rxFcurve.Eval(frame)
                            xforms["ry"] = ryFcurve.Eval(frame)
                            xforms["rz"] = rzFcurve.Eval(frame)
    
                            anim[frameString] = xforms

                            frame += step


                        # # Delete temp camera after plot.
                        si.DeleteObj(tmpBakedCam)
                        
                        camParm["anim"] = anim

                    # Write out file.
                    # outFile = self.ui.fileOutLocationLineEdit.text()
                    with open(outFile, "w") as outFile:
                        json.dump(camOut, outFile, sort_keys=True, indent=4)

                    # Turn logging back on.
                    self.logOn()
            
                    log("------------------------- Script has completed successfully! ------------------------")
                    self.ui.exportOutputMessageLabel.setText("Exported successfully!")
                    return True
                else:
                    self.ui.exportOutputMessageLabel.setText("Please specify a start and end frame range to bake.")
                    return False
            else:
                self.ui.exportOutputMessageLabel.setText("Please select one or more cameras and try again.")
                return False
        else:
            self.ui.exportOutputMessageLabel.setText("Please provide an output file. eg. /tmp/cameraFile.cam")
            return False

    def getKeys(self, selection):
        # initSel = disp("XSI.Collection")
        # initSel.SetAsText(oSel.GetAsText())

        # Temp storage for keys to dump into json.
        timeStorage = [posxt, posyt, poszt, rotxt, rotyt, rotzt]
        keyStorage = [posx, posy, posz, rotx, roty, rotz]

        for eachItem in selection:
            tmpPosX = eachItem.Kinematics.Local.Parameters("PosX")
            tmpPosY = eachItem.Kinematics.Local.Parameters("PosY")
            tmpPosZ = eachItem.Kinematics.Local.Parameters("PosZ")

            tmpRotX = eachItem.Kinematics.Local.Parameters("RotX")
            tmpRotY = eachItem.Kinematics.Local.Parameters("RotY")
            tmpRotZ = eachItem.Kinematics.Local.Parameters("RotZ")

            cacheParams = [tmpPosX, tmpPosY, tmpPosZ, tmpRotX, tmpRotY, tmpRotZ]

            i = 0
            for eachParam in cacheParams:
                fcurve = disp(eachParam.Source)
                if not fcurve:
                    log("no fcurve found for " + eachParam.FullName, 4)
                    return False

                log("\n" + eachParam.FullName)

                time = 0
                value = 0

                timeArray = []
                valueArray = []

                minKey = fcurve.GetMinKeyFrame()
                maxKey = fcurve.GetMaxKeyFrame()
                keys = fcurve.GetKeysBetween(minKey, maxKey)
                for eachKey in keys:
                    time = eachKey.Time
                    value = eachKey.Value
                    # log("time: " + str(time) + ", value: " + str(value))

                    timeArray.append(round(time, 3))
                    valueArray.append(value)

                # tmp = "+++===+++===+++ " + str(i) + ": " + str(timeArray)
                timeStorage[i] = timeArray
                keyStorage[i] = valueArray

                i = i + 1

                # log("+++===+++===+++ " + str(i) + ": " + str(timeStorage))
            # log("+++===+++===+++ " + str(i) + ": " + str(timeStorage))

        out = [timeStorage, keyStorage]
        return out

    def getKeyframeRange(self, selection):
        mySel = si.ActiveSceneRoot.FindChild(str(selection))

        # Temp storage for keys to dump into json.
        # timeStorage = [posxt, posyt, poszt, rotxt, rotyt, rotzt]
        # keyStorage = [posx, posy, posz, rotx, roty, rotz]

        # Loop through each camera to grab data.
        # for eachItem in oSel2:
        tmpPosX = mySel.Kinematics.Local.Parameters("PosX")
        tmpPosY = mySel.Kinematics.Local.Parameters("PosY")
        tmpPosZ = mySel.Kinematics.Local.Parameters("PosZ")

        tmpRotX = mySel.Kinematics.Local.Parameters("RotX")
        tmpRotY = mySel.Kinematics.Local.Parameters("RotY")
        tmpRotZ = mySel.Kinematics.Local.Parameters("RotZ")

        cacheParams = [tmpPosX, tmpPosY, tmpPosZ, tmpRotX, tmpRotY, tmpRotZ]

        # Get min/max keys for object passed in.
        for eachParam in cacheParams:
            fcurve = disp(eachParam.Source)
            if not fcurve:
                log("no fcurve found for " + eachParam.FullName, 4)
                return False

            minKey = fcurve.GetMinKeyFrame()
            maxKey = fcurve.GetMaxKeyFrame()
            keys = fcurve.GetKeysBetween(minKey, maxKey)

        return [minKey, maxKey, keys]
                
        
    def getSceneCameras(self):
        # Select children. constants.siCameraPrimType
        myObj = oRoot.FindChildren("", constants.siCameraPrimType, "", True) # [Name], [Type], [Family], [Recursive]

        # Loop through children.
        for obj in myObj:
            # log(obj)
            if obj.Type == "camera":
                # log(str(obj) + " IS a camera")
                # objs.append(obj.FullName)
                self.ui.cameraList.addItem(str(obj))
            # else:
                # log(str(obj) + " is NOT a camera")

        # self.ui.cameraList.addItems(str(myObj))

    
    def logOff(msg = "Logging has been turned off"):
        """Turn off PPG inspection for the duration of the function."""
        pref = "preferences.Interaction.autoinspect" # "preferences.scripting.cmdlog, preferences.scripting.msglog, preferences.scripting.msglogverbose, preferences.scripting.cmdlog, preferences.Interaction.autoinspect"
        log(msg, 32)
        si.SetValue(pref, False)

    def logOn(msg = "Logging has been turned on"):
        """Turn on PPG inspection back on after performing a task."""
        pref = "preferences.Interaction.autoinspect" # "preferences.scripting.cmdlog, preferences.scripting.msglog, preferences.scripting.msglogverbose, preferences.scripting.cmdlog, preferences.Interaction.autoinspect"
        si.SetValue(pref, True)
        log(msg, 32)

    def setFileInLocation(self):
        self.fileInLocationLineEdit.setText(self.openFiles())

    def setFileOutLocation(self):
        self.fileOutLocationLineEdit.setText(self.saveFiles())

    def openFiles(self, startDir=None):
        """Return the path to a file."""
        options  = QtGui.QFileDialog.DontUseNativeDialog 
        existing = self.fileOutLocationLineEdit.text()

        if len(existing):
            startDir = "//tela/trsam/dome/won/"
        else:
            startDir = existing

        filename = QtGui.QFileDialog.getOpenFileName(self, 'Select file', startDir, 'cam (*.cam)', options=QtGui.QFileDialog.DontUseNativeDialog)

        return filename

    def saveFiles(self, startDir=None):
        """Return the path to a file."""
        options  = QtGui.QFileDialog.DontUseNativeDialog 
        existing = self.fileOutLocationLineEdit.text()

        if len(existing):
            startDir = "//tela/trsam/dome/won/"
        else:
            startDir = existing

        filename = QtGui.QFileDialog.getSaveFileName(self, 'Select file', startDir, 'cam (*.cam)', options=QtGui.QFileDialog.DontUseNativeDialog)

        return filename

        
    def showAbout(self):
        box = QtGui.QMessageBox(self)
        box.setWindowTitle('About JSON Cam')
        box.setText('<html><i>JSON Cam is a small utility to import/export camera data software agnostically. Fuck FBX.<br/><br/>version: %s<br/>June 26, 2013 <a href="http://www.genecrucean.com" style="color: #A1BDC7">Gene Crucean</a><br/><br/>Python 2.6.6  |   PyQt 4.9.4  |  QtDesigner  |  PyQtForSoftimage <br/></i><html>' %__version__)
        box.setStyleSheet('QPushButton{height: 20px; width: 50px}')
        box.show()
        
        
    def log(self, text):
        Application.LogMessage(text)
        
        
def XSILoadPlugin( in_reg ):
    in_reg.Name = 'JSONCam'
    in_reg.Author = 'Gene Crucean'
    in_reg.Email = 'genecrucean@gmail.com'
    in_reg.Major = 1
    in_reg.Minor = 0
    in_reg.RegisterCommand('JSONCam')
    in_reg.RegisterMenu(constants.siMenuTbRenderPassEditNewPassID,"JSONCam_Menu",False,False)

def JSONCam_Menu_Init( in_ctxt ):
	oMenu = in_ctxt.Source
	oMenu.AddCommandItem("JSONCam","JSONCam")
	return True

def JSONCam_Execute():
    srcDir = ''
    for plugin in Application.Plugins:
        if plugin.Name == 'JSONCam':
            srcDir = os.path.dirname(plugin.Filename)
            
    uifilepath = os.path.join(srcDir,'JSONCam.ui')
    
    sianchor = Application.getQtSoftimageAnchor()
    sianchor = sip.wrapinstance( long(sianchor), QWidget )
    
    for child in sianchor.children(): # Prevent multiple instances.
        if isinstance(child, JSONCam ):
            return
    dialog = JSONCam(uifilepath, sianchor)  
    dialog.show()




